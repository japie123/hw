function [precision, recall, f1] = bench(ours, correct)
    hit = size(intersect(ours, correct), 2);
    miss = size(correct, 2) - hit;
    false_positive = size(ours, 2) - hit;

    precision = hit / (hit + false_positive);
    recall = hit / (hit + miss);
    f1 = 2 * precision * recall / (precision + recall);
end