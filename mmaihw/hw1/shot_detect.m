function main = shot_detect( video_number, bin_type, s ,thres)
  %UNTITLED2 Summary of this function goes here
  %   Detailed explanation goes here
  global H_BINS S_BINS V_BINS THRESHOULD_FADE 
  global cached cache cached_fade cache_fade
  global fid fid_f SUM_L2D Q166 TEST SUM spaceName 
  BIN_SIZE = 20;
  if(bin_type == 1) 
    H_BINS = 18;
    S_BINS = 4;
    V_BINS = 3;
    spaceName = 166
  else
    H_BINS = 16;
    S_BINS = 16;
    V_BINS = 16;
    spaceName = 16
  end
  SUM = s;
  Q166 = false;
  TEST = false;
  video_number
  if(~SUM)
    THRESHOULD = [thres(1) thres(2) thres(3)]
  else
    THRESHOULD = thres
  end
  % global THRESHOULD
  % Write out file to debug
  if(Q166)
    fName = 'diff_Q166.txt';
  else
    fName = 'diff.txt';
  end
  fRecord = 'avg_shot_length.txt';
  fid = fopen(strcat(int2str(video_number),int2str(spaceName)),'w');
  fid_f = fopen(strcat(int2str(video_number),int2str(spaceName),'_f'),'w');
  fR = fopen(fRecord,'at');
  f_process_cut = fopen('f_process_cut.txt','w');
  f_process_f = fopen('f_process_f.txt','w');
  fprintf(fR,'\n%d:\n',video_number);
  fprintf(fR,'H-%d,S-%d,V-%d \n',H_BINS,S_BINS,V_BINS);
  fprintf(fR,'SUM: %d\n',SUM);
  fprintf(fR,'THRESHOULD: ');
  fprintf(fR,'%f ',THRESHOULD);
  fprintf(fR,'\n');

  path = sprintf('./videos.hw01/%02d_frame/',video_number)
  cached = size( dir(strcat(path, int2str(video_number),'_cache_',int2str(spaceName),'.mat')) , 1) ~= 0
  cached_fade = size( dir(strcat(path, int2str(video_number),'_cache_fade_',int2str(spaceName),'.mat')) , 1) ~= 0
  cached_fade = 0;
  files = dir(strcat(path,'*.jpg'));
  if(TEST)
    test(video_number)
  else
    prev_frame = rgb2hsv( imread( strcat(path,'1.jpg') ) );
    if ~cached
      next_frame = zeros( size(prev_frame) );
      cache = zeros(size(files),1,3);
    else
      fprintf('load cache...\n');
      strcat(path , int2str(video_number), '_cache_',int2str(spaceName),'.mat')
      load( strcat(path , int2str(video_number), '_cache_',int2str(spaceName),'.mat') );
    end
    if ~cached_fade
      next_frame = zeros( size(prev_frame) );
      cache_fade = zeros(size(files),1,3);
    else
      fprintf('load cache_fade...\n');
      load( strcat(path , int2str(video_number), '_cache_fade_',int2str(spaceName),'.mat') );
      size(cache_fade)
    end

    cuts = [];
    fade = [];
    fade_status = 0;
    prev_hist = hist_hsv(prev_frame);
    fprintf(1, 'Executing\n');
    for i = 2 : size(files)-1 
      % pring progress
      if(mod(i,10) == 0)
        fprintf(2, '%d\n', i);
      else
        fprintf(2, '.');
      end
      % the working part
      next_frame = rgb2hsv( imread( strcat( path, int2str(i), '.jpg' ) ) );
      if( over_thres( prev_frame, next_frame, i,THRESHOULD) )
        cuts = [cuts i-1];
        fprintf(f_process_cut,'%d ',i-1);
        % fprintf(fid,'\n--\n%d\n--\n',cuts);
        fade_status = 0;
      else 
        % if( fade_status == 0 )
        %   prev_hist = hist_hsv(prev_frame);
        %   fade_status = 1;
        % elseif fade_status == 1 
        %   next_hist = hist_hsv(next_frame);
        %   if( over_thres_fade( prev_hist, next_hist, i) )
        %     cuts = [cuts i-1];            
        %     fprintf(f_process_cut,'%d ', i-1);
        %     fprintf(f_process_f,'%d ', i-1);
        %     fade = [fade i-1];
        %     fade_status = 0;
        %   end
        % end
      end
      prev_frame = next_frame;
    end
    if(~cached)
      % save the cache into a file
      'saveeee'
      size(cache)
      save(strcat(path, int2str(video_number),'_cache_',int2str(spaceName),'.mat'), 'cache');
    end
    if(~cached_fade)
      % save the cache into a file
      save(strcat(path, int2str(video_number),'_cache_fade_',int2str(spaceName),'.mat'), 'cache_fade');
    end
    fprintf('Done.\n');
    %  final output
    fprintf(fR,'cut:\n');
    fprintf(fR,'%d ',cuts);
    fprintf(fR,'fade:\n');
    fprintf(fR,'%d ',fade);
    [precision, recall, f1] = bench(cuts, correct_cuts(video_number));
    fprintf(fR,'\nprecision:%d\n',precision);
    fprintf(fR,'recall:%d\n', recall);
    fprintf(fR,'f1:%d\n', f1);
    cuts = [1 cuts  size(files)];
    shot_length = abs(circshift(cuts,  [0,1]) - cuts);
    avg_shot_length = mean( shot_length(2:end) );
    fprintf(fR,'avg_shot_length: %d\n',avg_shot_length); 
  end
  fclose('all');
end

function test(video_number)
  path = sprintf('./videos.hw01/%02d_frame/',video_number);
  prev_frame = rgb2hsv( imread( strcat(path,'473.jpg') ) );
  next_frame = rgb2hsv( imread( strcat(path,'474.jpg') ) );
  if( over_thres( prev_frame, next_frame, 1) )
    'lala'
  end
end


function over = over_thres(prev_frame,next_frame,i,THRESHOULD)
  global cached cache fid TEST SUM
  if or(~cached,TEST)
    prev_hist = hist_hsv(prev_frame);

    next_hist = hist_hsv(next_frame);
    diff_hist = next_hist - prev_hist;
    d = diff_hist .* diff_hist;
    L = sum(sum(d(:,:,:),1));
    if(SUM == 1)
      L2Dis = L(:,:,1) + L(:,:,2) + L(:,:,3);
    else
      L2Dis = [L(:,:,1),L(:,:,2),L(:,:,3)];
    end
    fprintf(fid,'%d:\n',i);
    fprintf(fid,'L2D: %d\r\n', L2Dis); 
    fprintf(fid,'====\n');
    % size(cache(i,:))
    if ~cached
      cache(i,:) = L2Dis;
    end
  else
    L = cache(i,:);
    if(SUM == 1)
      L2Dis = L(1) + L(2) + L(3);
    else
      L2Dis = [L(1) L(2) L(3)];
    end
  end
  if( L2Dis > THRESHOULD )
    over = true;
  else
    over = false;
  end
end

function over = over_thres_fade(prev_hist,next_hist,i)
  global THRESHOULD_FADE fid_f TEST cached_fade cache_fade SUM video_number spaceName
  if or(~cached_fade,TEST)
    diff_hist = next_hist - prev_hist;
    d = diff_hist .* diff_hist;
    L = sum(sum(d(:,:,:),1));
    if(SUM == 1)
      L2Dis = L(:,:,1) + L(:,:,2) + L(:,:,3);
    else
      L2Dis = [L(:,:,1),L(:,:,2),L(:,:,3)];
    end
    fprintf(fid_f,'%d:\n',i);
    fprintf(fid_f,'L2D: %d\r\n', L2Dis); 
    fprintf(fid_f,'====\n');
    if ~cached_fade
      cache_fade(i,:) = L2Dis;
    end
  else
    L2Dis = cache_fade(i,:);
  end
  if( L2Dis > THRESHOULD_FADE )
    over = true;
  else
    over = false;
  end
end

function  histogram = hist_hsv(frame)
  global H_BINS S_BINS V_BINS Q166
  histogram = zeros(H_BINS, S_BINS, V_BINS);
  % Applying Q166 quantizer (18 hues, 3 saturation, 3val, 4gray).
  q_frame(:,:,1) = floor(frame(:,:,1) .* H_BINS);
  q_frame(:,:,2) = floor(frame(:,:,2) .* S_BINS);
  q_frame(:,:,3) = floor(frame(:,:,3) .* V_BINS);
  % Find grey pixels (pixels with low saturation).
  idx = (q_frame(:,:,2) == 0);
  % fprintf('idx:%d',idx) 

  if(Q166)
    fprintf('Q166:\n');
    logical0s = false(size(frame(:,:,1)));
    % indexing logical selecting value (:,:,3)
    idx3_v = logical0s; idx3_v(:,:,2) = logical0s; idx3_v(:,:,3) = idx;
    % For the 'value' of grey pixels, use a 4-step quantizer instead.
    % However the quantized value is going to be passed into the median
    % filter, along with non-grey pixels, thus we should normalize the
    % value to 0~3 (with decimal points) so that it matches the range
    % of other non-grey pixels.
    q_frame(idx3_v) = floor(frame(idx3_v) .* 4) .* 3/4;

    %% Median Filtering
    % non-linear filter (2D median filter)
    % eliminate the effect of extreme values
    filt_im(:,:,1) = medfilt2(q_frame(:,:,1));
    filt_im(:,:,2) = medfilt2(q_frame(:,:,2));
    filt_im(:,:,3) = medfilt2(q_frame(:,:,3));
    
    %% Histogram calculation
    % Find the grey pixels after median filtering, 
    % since they should be seperated from other pixels when calculating
    % histograms
    idx = (filt_im(:,:,2) == 0);
    % Grey pixel indexing logical 'idx' changed; 
    % rebuild idx3_v.
    idx3_v(:,:,3) = idx;
    
    % Bulding indexing logicals of non-grey pixel's H, S, V values
    nidx = not(idx);
    nidx3_h = nidx; nidx3_h(:,:,2) = logical0s; nidx3_h(:,:,3) = logical0s;
    nidx3_s = logical0s; nidx3_s(:,:,2) = nidx; nidx3_s(:,:,3) = logical0s;
    nidx3_v = logical0s; nidx3_v(:,:,2) = logical0s; nidx3_v(:,:,3) = nidx;
    
    % 3-D histogram initialization
    zero18x3 = zeros(18,3); 
    hist = zero18x3; hist(:,:,2) = zero18x3; hist(:,:,3) = zero18x3;

    % non_grey is a 2-D matrix that hosts all non-grey pixels,
    % each pixel is a row vector of its H, S and V values
    non_grey = [filt_im(nidx3_h), filt_im(nidx3_s), filt_im(nidx3_v)];
    
    % Histogram calculation
    for pixel = transpose(non_grey)
        % notice that even after values are floor'ed,
        % there might be some values = upperbound, and should be
        % saturated to upperbound-1.
        h = min(pixel(1), 17) + 1;
        s = min(pixel(2)-1, 2) + 1; % s = 1~3
        v = min(floor(pixel(3)), 2) + 1;
            % decimal of pixel(3) are caused by pixels that are 
            % 'used to' be grey before.
            
        hist( h, s, v ) = hist( h, s, v ) + 1;
    end
    
    assert(sum(sum(sum(hist))) == size(non_grey,1), ...
           'HSV bins should contain all non-grey pixels');
    
    % Quantize the original image value to calculate gray bins.
    % Since filt_im, q_im are already quantized to 3 steps,
    % we can only use original im to do the 4-step qunatization.
    gray_bins = sum(histc(floor(frame(idx3_v).*4), 0:4), 2);
    
    % histc puts values that equals the upper_limit to the last bin
    % and it should be in the bin of upper_limit-1~upper_limit.
    gray_bins = merge_last_bin(gray_bins);
       
    %% Feature Vector
    % Concatenate the bins to form a 166-dim feature vector.
    % Cache the feature vector
    s = size(frame);
    histogram = [hist(1:end), transpose(gray_bins)] ./ (s(1)*s(2));
    assert(sum(histogram) == 1, ...
           'Bins should cover all pixels');
  else
    % fprintf('q:\n');
    q_frame(:,:,1) = ceil(frame(:,:,1) .* H_BINS);
    q_frame(:,:,2) = ceil(frame(:,:,2) .* S_BINS);
    q_frame(:,:,3) = ceil(frame(:,:,3) .* V_BINS);
    idx = (q_frame == 0);
    q_frame(idx) = 1;

    [h w c] = size(frame);
    RUN = true;
    if (RUN)
      for i = 1:h
        for j = 1:w
          p = q_frame(i,j,:);
          histogram(p(:,:,1),p(:,:,2),p(:,:,3)) = histogram(p(:,:,1),p(:,:,2),p(:,:,3)) + 1;
        end
      end
    end
  end
end