function main( video_number )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
path = sprintf('./videos.hw01/%02d_frame/',video_number);
files = dir(strcat(path,'*.jpg'));
IMAGE_SIZE = [240, 320];
REGION_SIZE = [60, 80];
n_regions = prod( IMAGE_SIZE ./ REGION_SIZE );
count_thres = 0.5 * n_regions;
% files(1)
index = 1;


first_frame = imresize( ...
    imcrop( ...
        imread(strcat(path,int2str(index),'.jpg')), ...
        IMAGE_SIZE ...
    ) ...
);
% first_frame(2)
first_frame = rgb2hsv(first_frame);

[height, width, channel] = size(first_frame);
last_hists = regionhist(first_frame);
cut = [];


for i = index + 1 : size(files)-1
    if(mod(i,10) == 0)
        fprintf(2, '%d ',i);
    else
        fprintf(2,'.');
    end
    
    im = imread(strcat(path, int2str(i), '.jpg'));
    
    
    this_hists = regionhist(im);
    diff_hist = last_hist - this_hist;
    L2Dists = sum(diff_hist .* diff_hist);
    hist_intersect(this_hists)
    
    n_reg_below_diff_thres = transpose(histc(L2Dists, edges));
    n_reg_below_diff_thres = n_reg_below_diff_thres(1);        

    
    if(n_regions - n_reg_below_diff_thres >= count_thres)
        cuts = [cuts i];
    end
    

end


end

function hists = regionhist(img)
    global n_region n_bins REGINE_SIZE;
    
    [height, width, ~] = size(img);
    hists = zeros(n_regions, n_bins);
    index = 1;
    for i = 1:REGINE_SIZE(1):height-1
        for j = 1:REGINE_SIZE(1):weight-1
            hist(index,:) = myhist(img(i:i+REGION_SIZE(1)-1, j:j+REGION_SIZE(2)-1,:))
        end
    end
    
     
end

function h = myhist(region)
    edges = 0:16:256;
    histc(region,edges)
    hh = sum(histc(region,edges),2);
    
%     h = [trans]
end