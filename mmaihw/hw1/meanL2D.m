function thres = meanL2D(video_number,spaceName)
  path = sprintf('./videos.hw01/%02d_frame/',video_number);
  fprintf('load cache...\n');
  file_name = strcat(path , int2str(video_number), '_cache_',int2str(spaceName),'.mat');
  load(file_name);

  % for i = 1:size(dir(file_name))
  %   mean(cache(i,:))
  % end
  fRecord = 'threshould.txt';
  fR = fopen(fRecord,'at');
  m = mean(cache);
  standard = std(cache);
  thres = m + 0.5 .* standard;
  fprintf(fR,'%d - %d\n',video_number,spaceName);
  fprintf(fR,'m: %d, std:%d, thres:%d\n',m,standard,thres);
  threAveage = (thres(:,:,1)+thres(:,:,2)+thres(:,:,3))/3;
  fprintf(fR,'Avearage: %d',threAveage);
  fprintf(fR,'\n');
end