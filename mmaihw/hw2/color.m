function ret = color(file_path)
  % resizing const
  W = 128; H = 128;
  % 18 hues, 3 saturation, 3val, 4gray
  H_BINS = 18; S_BINS = 4; V_BINS = 3;
  histogram = zeros(H_BINS, S_BINS, V_BINS);
  
  % splitting file path and get feature if one is saved before
  [pathstr, name, ~] = fileparts(file_path);
  cache_path = fullfile(pathstr, [name '.fv_color.mat']);
  is_cached = size(dir(cache_path), 1) ~= 0;
  
  %is_cached = false;
  if(is_cached)
    % feature cached, retrieve vector from feature file
    ret = load(cache_path);
    % fprintf(' ret%d; ',length(ret))
    ret = ret.ret;

  else
    im = imresize( imread( file_path ), [ H, W ] );
    % if only contains 1 channel, let G,B  be smae with R
    if(size(im, 3) == 1) 
       im(:,:,2) = im(:,:,1);
       im(:,:,3) = im(:,:,1);
    end
    im = rgb2hsv( im );

    % Applying Q166 quantizer (18 hues, 3 saturation, 3val, 4gray).
    q_im(:,:,1) = ceil(im(:,:,1) .* H_BINS);
    q_im(:,:,2) = ceil(im(:,:,2) .* S_BINS);
    q_im(:,:,3) = ceil(im(:,:,3) .* V_BINS);
    
    
    
    % zero18x3 = zeros(18,3); 
    % hist = zero18x3; hist(:,:,2) = zero18x3; hist(:,:,3) = zero18x3;

    % nidx = not(idx);
    % nidx3_h = nidx; nidx3_h(:,:,2) = logical0s; nidx3_h(:,:,3) = logical0s;
    % nidx3_s = logical0s; nidx3_s(:,:,2) = nidx; nidx3_s(:,:,3) = logical0s;
    % nidx3_v = logical0s; nidx3_v(:,:,2) = logical0s; nidx3_v(:,:,3) = nidx;
    
    % non_grey = [q_im(nidx3_h), q_im(nidx3_s), q_im(nidx3_v)];
    
    % figure(1); imshow(q_im(:,:,1), [0,18]);
    % figure(2); imshow(q_im(:,:,2), [0,18]);
    % figure(3); imshow(q_im(:,:,3), [0,18]);

    ret = [];
    % cut 4*4 region
    for m = 1:4
      for n = 1:4
        m_ind = (m-1)*H/4;
        n_ind = (n-1)*H/4;
          gray_bins = zeros(1,5);
          for i = 1:H/4
            for j = 1:W/4
      
              histogram = zeros(H_BINS, S_BINS, V_BINS);
              p = q_im(i+m_ind,j+n_ind,:);

              h = min(p(:,:,1), 17) + 1;
              s = min(p(:,:,2)-1, 2) + 1; % s = 1~3
              v = min(floor(p(:,:,3)), 2) + 1;
              if s ~= 0
                histogram(h,s,v) = histogram(h,s,v) + 1;
              else
                v = floor(im(i+m_ind,j+n_ind,3) *4 );
                gray_bins(v+1) = gray_bins(v+1) + 1;
              end
            end
          end
          gray_bins = merge_last_bin(gray_bins);
          ret = [ret histogram(1:end) gray_bins];
      end
    end
    % fprintf('%d ',length(ret));

    % caching
    save(cache_path, 'ret');   
  end
  % fclose(fid_color);
end

function ret = merge_last_bin(h)
  % merge the last bin of the histogram into the second last bin of
  % histogram, and return 1~end-1 part of histogram.
  h(end-1) = h(end-1) + h(end);
  ret = h(1:end-1);
end