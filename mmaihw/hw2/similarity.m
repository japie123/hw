function ret = similarity(v1, v2)
  % length(v1)
  % length(v2)
  d= v1 - v2;
  ret = d * d';
end