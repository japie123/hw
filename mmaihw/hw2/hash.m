function db = hash(db)
  % use color feature to hash
  % [path_str, name, ~] = fileparts(file_path);
  cache_path = fullfile('.', ['hash_code.mat']);
  is_cached = size(dir(cache_path), 1) ~= 0;
  d = length(db(1).color);
  dimentions = [d d/2.0 d/3.0];
  if(is_cached)
    ret = load(cache_path);
    db = ret.db;
  else
    for d = 1:3
      k_bit = dimentions(d);
      fprintf(1,'dim: %d \n start hash_code:\n',k_bit);
      hash_code_tmp = cell(1,numel(db));
      for j = 1:k_bit
        w = getW(length(db(1).color));
        for i = 1:numel(db)
          % length(db(i).color)
          h = (dot(w,db(i).color)+1)/2;
          if h >= 0
            hash_code_tmp{i} = [hash_code_tmp{i} 1];
          else
            hash_code_tmp{i} = [hash_code_tmp{i} 0];
          end
        end
        fprintf(1,'.');
        if mod(j,50) == 0
          fprintf(1,'\n%d',j+1);
        end
      end
      for i = 1:numel(db)
        db(i).hash_code = [db(i).hash_code {hash_code_tmp{i}}];
      end
      fprintf(1,'hash_code - done');
    end
  end
  save(cache_path, 'db');    
  % db(1).hash_code
end

function w = getW(dimention)
  w = [];
  for i = 1:dimention
    w = [w normrnd(0,1)]; 
  end
end