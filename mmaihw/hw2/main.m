
function main = main()
  % path = sprintf('./dataset/%s/',dir_name)
  % files = dir(strcat(path,'*.jpg'));
  % fprintf(fid,'lalala\n')
  % fid_color = fopen('output_tmp','w');

  NUMBER_IMG_SAME_TYPE = 20;
  query_list = [{'flower'} {'bicycle'} {'bus'} {'bird'}];
  % �??�??夾�???0張�?
  queries = struct('dir_name', {}, 'name', {}, 'color', {}, 'texture', {}, 'idx', {});
  [db, queries] = load_feature('dataset/', @(d) d.isdir && d.name(1) ~= '.', ...
    @(d) length(find(ismember(query_list, d.name))) ~= 0 );
  % queries(1)
  last_dir_name = queries(1).dir_name;
  Precision = [];
  Recall = [];
  % the index for query in the same dir
  plot_ind = 1;
  type_ind = 1;
  db = hash(db);

  for i = 1 : length(queries)
    fprintf(1,'For query: %s\n',queries(i).name);
    if ~(strcmp(last_dir_name, queries(i).dir_name))
      plot_ind = plot_PR_curve(Precision,Recall,NUMBER_IMG_SAME_TYPE,plot_ind,last_dir_name);
      % reset the P/R
      Precision = [];
      Recall = [];
      plot_ind = plot_ind + 1;
      last_dir_name = queries(i).dir_name;
      type_ind = 1;
    end
    % fprintf(fid, '%s \n', queries(i).name);
    q_dir_name = queries(i).dir_name;
    q_name = queries(i).name;
    q_color = queries(i).color;
    q_texture = queries(i).texture;
    q_idx = queries(i).idx;
    result = struct('name', {}, 'dist_color', {}, 'dist_texture', {}, 'dist_hash_code_k1', {}, 'dist_hash_code_k2', {}, 'dist_hash_code_k3', {});
    % result.dist_hash_code = cell(1,3);
    % ==========================================================
    % color and texture
    % ==========================================================
    % fprintf(1,'Processing: color, texture, hash...\n');
    for j = 1:numel(db)
      entry = db(j);
      result(j).dir_name = entry.dir_name;
      result(j).name = entry.name;
      result(j).dist_color = similarity(q_color, entry.color);
      result(j).dist_texture = similarity(q_texture, entry.texture);
      % hash
      result(j).dist_hash_code_k1 = pdist2(db(q_idx).hash_code{1}, entry.hash_code{1}, 'hamming');
      result(j).dist_hash_code_k2 = pdist2(db(q_idx).hash_code{2}, entry.hash_code{2}, 'hamming');
      result(j).dist_hash_code_k3 = pdist2(db(q_idx).hash_code{3}, entry.hash_code{3}, 'hamming');
    end
    % result
    % index_of: ?��??�第幾�?�?????�?????
    [~, c_index_of] = sort([result.dist_color]);
    [~, t_index_of] = sort([result.dist_texture]);
    % hash
    [~, h_index_of] = sort([result.dist_hash_code_k1]);
    [Precision{type_ind,4}, Recall{type_ind,4}] = record_PR(result, h_index_of, q_name, q_dir_name, queries);
    [~, h_index_of] = sort([result.dist_hash_code_k2]);
    [Precision{type_ind,5}, Recall{type_ind,5}] = record_PR(result, h_index_of, q_name, q_dir_name, queries);
    [~, h_index_of] = sort([result.dist_hash_code_k3]);
    [Precision{type_ind,6}, Recall{type_ind,6}] = record_PR(result, h_index_of, q_name, q_dir_name, queries);
    % record this P/R of this type
    [Precision{type_ind,1}, Recall{type_ind,1}] = record_PR(result, c_index_of, q_name, q_dir_name, queries);
    [Precision{type_ind,2}, Recall{type_ind,2}] = record_PR(result, t_index_of, q_name, q_dir_name, queries);    

    % ==========================================================
    % mixed rank, the original distance is added with th rank 
    % ==========================================================
    % fprintf(1,'Processing: mixed...\n');
    rank_mixed = zeros(1,numel(db));
    for rank = 1:numel(rank_mixed) % for each rank
      % c_index_of(n) is the result id that ranked n in color feature
      % distance.
      % t_index_of(n) is the result id that ranked n in texture
      % feature distance.
      
      rank_mixed(c_index_of(rank)) = ...
          rank_mixed(c_index_of(rank)) + rank;
      rank_mixed(t_index_of(rank)) = ...
          rank_mixed(t_index_of(rank)) + rank; 
    end
    % rank_mixed_2 = zeros(1,numel(db));
    % rank_mixed_2 = c_index_of + t_index_of;
    % fprintf(1,'compareee');
    % rank_mixed == rank_mixed_2
    [~, m_index_of] = sort(rank_mixed);
    [Precision{type_ind,3}, Recall{type_ind,3}] = record_PR(result, m_index_of, q_name, q_dir_name, queries);


    type_ind = type_ind + 1;
    % break;
  end
  % for last query type
  plot_ind = plot_PR_curve(Precision,Recall,NUMBER_IMG_SAME_TYPE,plot_ind,last_dir_name);

  % fclose(fid)

end


function [db, queries] = load_feature(target_dir, dir_filter, query_filter)
    % fid = fopen('output_tmp','w');

    % target_dir should include trailing slash.
    % dir_filter should return true on directories that we want to
    %            traverse.
    queries = struct('dir_name', {}, 'name', {}, 'color', {}, 'texture', {});
    
    dd = dir(strcat(target_dir, '*'));
    db = struct('dir_name', {}, 'name', {}, 'color', {}, 'texture', {}, 'hash_code', {});
    idx = 1;
    idx_q = 1;
    fprintf(1,'Loading feature');
    for i = 1:numel(dd)
        d = dd(i);
        if ~( dir_filter(d) )
          continue;
        end

        % fprintf(fid,'%s\n',d.name(1));
        path = strcat(target_dir, d.name);
        files = dir(strcat(path, '/*g')); % jpg / jpeg
        for j = 1:numel(files)
          f = files(j);
          db(idx).dir_name = d.name;
          % db(idx).dir_name
          db(idx).name = strcat(path, '/', f.name);
          db(idx).color = color(db(idx).name);
          db(idx).texture = texture(db(idx).name);
          if ( query_filter(d) )
            queries(idx_q).dir_name = d.name;      
            queries(idx_q).name = db(idx).name; 
            queries(idx_q).color = db(idx).color; 
            queries(idx_q).texture = db(idx).texture;
            queries(idx_q).idx = idx;
            idx_q = idx_q + 1;
          end
          idx = idx + 1;
          fprintf(2,'.');
        end
        % JPG
        files = dir(strcat(path, '/*G')); % jpg / jpeg
        for j = 1:numel(files)
          f = files(j);
          db(idx).dir_name = d.name;
          % db(idx).dir_name
          db(idx).name = strcat(path, '/', f.name);
          db(idx).color = color(db(idx).name);
          db(idx).texture = texture(db(idx).name);
          if ( query_filter(d) )
            queries(idx_q).dir_name = d.name;      
            queries(idx_q).name = db(idx).name; 
            queries(idx_q).color = db(idx).color; 
            queries(idx_q).texture = db(idx).texture;
            queries(idx_q).idx = idx;
            idx_q = idx_q + 1;
          end
          idx = idx + 1;
          fprintf(2,'.');
        end        
    end
    fprintf(1, 'done.\n');
end

function [Precision, Recall] = record_PR(result, index_of, q_name, q_dir_name, queries)
  ind = 1;
  founded = 0;
  Recall = [];
  Precision = [];
  while (founded < 20)
    img = result(index_of(ind));
    if( strcmp(img.dir_name, q_dir_name))
      founded = founded + 1;
      Recall = [Recall (founded - 1)/20];
      Precision = [Precision (founded-1)/ind];
    end
    ind = ind + 1;
  end
  % ind
  Recall = Recall(2:length(Recall));
  Precision = Precision(2:length(Precision));
end

function plot_ind = plot_PR_curve(Precision, Recall, NUMBER_IMG_SAME_TYPE, plot_ind, last_dir_name)
  fprintf(1,'Processing: plot_PR_curve...\n');
  % fprintf(1,'plot_PR_curve');
  % average
  total_Precision = zeros(6,NUMBER_IMG_SAME_TYPE-1);
  total_Recall = zeros(6,NUMBER_IMG_SAME_TYPE-1);
  for j = 1:NUMBER_IMG_SAME_TYPE
    % color
    total_Precision(1,:) = total_Precision(1,:) + Precision{j,1};
    total_Recall(1,:) = total_Recall(1,:) + Recall{j,1}; 
    % texture
    total_Precision(2,:) = total_Precision(2,:) + Precision{j,2};
    total_Recall(2,:) = total_Recall(2,:) + Recall{j,2};
    % mixed
    total_Precision(3,:) = total_Precision(3,:) + Precision{j,3};
    total_Recall(3,:) = total_Recall(3,:) + Recall{j,3};
    % Hash
    total_Precision(4,:) = total_Precision(4,:) + Precision{j,4};
    total_Recall(4,:) = total_Recall(4,:) + Recall{j,4};
    total_Precision(5,:) = total_Precision(5,:) + Precision{j,5};
    total_Recall(5,:) = total_Recall(5,:) + Recall{j,5};
    total_Precision(6,:) = total_Precision(6,:) + Precision{j,6};
    total_Recall(6,:) = total_Recall(6,:) + Recall{j,6};
  end
  average_Precision = total_Precision ./ NUMBER_IMG_SAME_TYPE;
  average_Recall = total_Recall ./ NUMBER_IMG_SAME_TYPE;
  
  % plot this type of query image
  % color
  h=figure('visible','off');
  set(gcf, 'name', strcat('Color histogram: ',last_dir_name));
  plot(average_Recall(1,:),average_Precision(1,:));
  xlabel('Recall');
  ylabel('Precision');
  saveas(h,fullfile('figures',strcat('Color histogram: ',last_dir_name)),'jpg');
  % texture 
  h=figure('visible','off');
  set(gcf, 'name', strcat('Texture: ',last_dir_name));
  plot(average_Recall(2,:),average_Precision(2,:));
  xlabel('Recall');
  ylabel('Precision');
  saveas(h, fullfile('figures',strcat('Texture: ',last_dir_name)),'jpg');
  % mixed
  h=figure('visible','off');
  set(gcf, 'name', strcat('Mixed: ',last_dir_name));
  plot(average_Recall(3,:),average_Precision(3,:));
  xlabel('Recall');
  ylabel('Precision');
  saveas(h, fullfile('figures',strcat('Mixed: ',last_dir_name)),'jpg');
  % hashed
  h=figure('visible','off');
  set(gcf, 'name', strcat('Binary Hashing: ',last_dir_name));
  plot(average_Recall(4,:),average_Precision(4,:),'-r', ...
    average_Recall(5,:),average_Precision(5,:), '-b', ...
    average_Recall(6,:),average_Precision(6,:), '-g' ...
    );
  legend('k = d','k = d/2','k = d/3');
  xlabel('Recall');
  ylabel('Precision');
  saveas(h, fullfile('figures',strcat('Binary Hashing: ',last_dir_name)),'jpg');
end